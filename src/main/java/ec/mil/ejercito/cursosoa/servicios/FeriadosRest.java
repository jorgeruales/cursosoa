/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import ec.mil.ejercito.cursosoa.dao.FeriadosFacade;
import ec.mil.ejercito.cursosoa.modelo.Feriados;
import ec.mil.ejercito.cursosoa.util.Feriado;
import ec.mil.ejercito.cursosoa.util.RespuestaFeriados;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author george
 */
@Path("/listarFeriados")
@Stateless
public class FeriadosRest {
    @EJB 
    protected FeriadosFacade feriadosFacade;
    
    @GET
    @Path("/{ciclo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMsg(@PathParam("ciclo") String msg) {
        RespuestaFeriados respuestaFeriados = new RespuestaFeriados();
        respuestaFeriados.setCodigo("2000");
        respuestaFeriados.setMensaje("Items obtenidos con éxito");
        respuestaFeriados.setListaFeriados(new ArrayList<Feriado>());
        
        List<Feriados> listaFeriadosBD = feriadosFacade.findAll();
        
        for(Feriados feriadoBD: listaFeriadosBD){
            Feriado feriadoActual = new Feriado();
            feriadoActual.setAnio(feriadoBD.getCiclo());
            feriadoActual.setDescrpcion(feriadoBD.getDescripcion());
            feriadoActual.setDiaDelAnio(feriadoBD.getDiaAnio());
            feriadoActual.setFechaFeriado(feriadoBD.getFechaFeriado().toString());
            respuestaFeriados.getListaFeriados().add(feriadoActual);
        }        
    
        return Response.status(200).entity(respuestaFeriados).build();

    }
    
}
