/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import ec.mil.ejercito.cursosoa.dao.VisitasFacade;
import ec.mil.ejercito.cursosoa.modelo.Visitas;
import ec.mil.ejercito.cursosoa.util.ReservacionHotel;
import ec.mil.ejercito.cursosoa.util.RespuestaServicio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author george
 */
@WebService(serviceName = "ListarReservas")
public class ListarReservas {

    @EJB
    protected VisitasFacade visitasFacade;
    
    @WebMethod(operationName = "mostrarLista")
    public RespuestaServicio mostrarLista() {
        RespuestaServicio respuesta = new RespuestaServicio();
        respuesta.setCodigo("200");
        respuesta.setMensaje("Respuesta obtenida correctamente");
        respuesta.setObservacion("La lista perteece a los hoteles de Ecuador");
        respuesta.setListadoReservasTuristicos(new ArrayList<ReservacionHotel>());
        
        for(int i = 0 ; i<20; i++){
            ReservacionHotel reserva = new ReservacionHotel();
          //  reserva.setCantiadPersonas(Math.random()+"");
            reserva.setFechaIngreso(new Date()+"");
            reserva.setFechaSalida(new Date()+"");
            reserva.setHotel("Marques");
            reserva.setTitularReserva("JR "+new Date().getTime());        
            respuesta.getListadoReservasTuristicos().add(reserva);
        }
        
        return respuesta;
    }
    
    
    @WebMethod(operationName = "mostrarListaBD")
    public RespuestaServicio mostrarListaBD() {
        RespuestaServicio respuesta = new RespuestaServicio();
        respuesta.setCodigo("200");
        respuesta.setMensaje("Respuesta obtenida correctamente");
        respuesta.setObservacion("La lista perteece a los hoteles de Ecuador");
         respuesta.setListadoReservasTuristicos(new ArrayList<ReservacionHotel>());
         // obtenemos informacion de la base de datos
        List <Visitas> listaVisitasBdd = visitasFacade.findAll();
        
        // llenamos los objetos de respuesta con lo obtenido de BDD
        for(Visitas visitaItem:listaVisitasBdd){
            ReservacionHotel reserva = new ReservacionHotel();
            reserva.setCantiadPersonas((visitaItem.getVisHabitacionesReserva() * 2));
            reserva.setFechaIngreso(visitaItem.getVisFechaIngreso()==null ? "NO INGRESO" : visitaItem.getVisFechaIngreso().toString());
            reserva.setFechaSalida(visitaItem.getVisFechaSalida()==null ? "NO SALIDA" : visitaItem.getVisFechaSalida().toString());
            reserva.setHotel("Joan Sebastian - Atacames");
            reserva.setTitularReserva(visitaItem.getVisClienteCedula());        
            respuesta.getListadoReservasTuristicos().add(reserva);        
        }

        return respuesta;
    }



}
