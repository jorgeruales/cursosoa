/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import ec.mil.ejercito.cursosoa.dao.VisitasFacade;
import ec.mil.ejercito.cursosoa.modelo.Visitas;
import ec.mil.ejercito.cursosoa.util.ReservacionHotel;
import ec.mil.ejercito.cursosoa.util.RespuestaServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author george
 */
@Path("/listarReservas")
@Stateless
public class ListarReservasRest {

    @EJB
    protected VisitasFacade visitasFacade;

    @GET
    @Path("/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarReservaciones(@PathParam("cedula") String cedula) {

        RespuestaServicio respuesta = new RespuestaServicio();
        respuesta.setCodigo("200");
        respuesta.setMensaje("Respuesta obtenida correctamente");
        respuesta.setListadoReservasTuristicos(new ArrayList<ReservacionHotel>());
        // obtenemos informacion de la base de datos
        List<Visitas> listaVisitasBdd = visitasFacade.obtenerPorCedula(cedula);

        // controlar mensaje cuando está vacio
        if (listaVisitasBdd.isEmpty()) {
            respuesta.setObservacion("No existe resarvaciones para esa cedula");
        } else {
            respuesta.setObservacion("La lista pertenece al uauario: "+cedula);
        }

        // llenamos los objetos de respuesta con lo obtenido de BDD
        for (Visitas visitaItem : listaVisitasBdd) {
            ReservacionHotel reserva = new ReservacionHotel();
            reserva.setCantiadPersonas((visitaItem.getVisHabitacionesReserva() * 2));
            reserva.setFechaIngreso(visitaItem.getVisFechaIngreso() == null ? "NO INGRESO" : visitaItem.getVisFechaIngreso().toString());
            reserva.setFechaSalida(visitaItem.getVisFechaSalida() == null ? "NO SALIDA" : visitaItem.getVisFechaSalida().toString());
            reserva.setHotel("Joan Sebastian - Atacames");
            reserva.setTitularReserva(visitaItem.getVisClienteCedula());
            respuesta.getListadoReservasTuristicos().add(reserva);
        }

        return Response.status(200).entity(respuesta).build();
    }

}
