/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import ec.mil.ejercito.cursosoa.util.Auto;
import ec.mil.ejercito.cursosoa.util.RespuestaServicioAutos;
import java.util.ArrayList;
import java.util.Date;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author george
 */
@WebService(serviceName = "ListarAutos")
public class ListarAutos {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "decirHola")
    public String decirHola(@WebParam(name = "nombreASaludar") String nombreASaludar) {
        return "Hola " + nombreASaludar + ", bienvenido!";
    }
    
    
    
     /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "listarAutosDisponibles")
    public RespuestaServicioAutos listarAutosDisponibles() {
        RespuestaServicioAutos respuestaServcio = new RespuestaServicioAutos();
        respuestaServcio.setMensaje("La informacion fue obtenida correctamente");
        respuestaServcio.setCodigo("200");
        respuestaServcio.setObservacion("N/A");
        
        respuestaServcio.setListadoAutos(new ArrayList<Auto>());
        for(int i =0; i<20; i++){
            Auto autoActual = new Auto();
            autoActual.setAnio(1999+i);
            autoActual.setColor("Rojo"+new Date().getTime());
            autoActual.setMarca("Audi "+new Date().getTime());
            autoActual.setPlaca("PBD-"+new Date().getTime());
            autoActual.setListaExtras(new ArrayList<java.lang.String>());
            for(int j =0 ; j< 5 ; j++){
                autoActual.getListaExtras().add("Accesorio "+i+" - "+j);
            }            
            respuestaServcio.getListadoAutos().add(autoActual);
        
        }
       
        return respuestaServcio;
    }
    
    
    
    
}
