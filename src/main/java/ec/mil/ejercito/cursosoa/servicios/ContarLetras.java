/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author george
 */
@WebService(serviceName = "contarLetras")
public class ContarLetras {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "contar")
    public String contar(
            @WebParam(name = "textoContar") String textoContar,
            @WebParam(name = "caracterContar") String caracterContar) {
        char letraContar = caracterContar.charAt(0);
        int numero = 0;
        for (char letra : textoContar.toCharArray()) {
            if (letra == letraContar) {
                numero++;
            }
        }

        return "La letra "+letraContar+" existe : " + numero+ " veces";
    }
}
