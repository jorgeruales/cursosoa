/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.servicios;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author george
 */
@WebService(serviceName = "Saludar")
public class Saludar {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "decirHola")
    public String hello(@WebParam(name = "persona") String persona) {
        return "Hola " + persona + " !, he registrado tu saludo a las:  "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
    }
}
