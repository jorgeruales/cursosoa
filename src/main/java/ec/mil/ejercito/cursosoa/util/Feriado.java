/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.util;

/**
 *
 * @author george
 */
public class Feriado {
    
    public int anio;
    public int diaDelAnio;
    public String descrpcion;
    public String fechaFeriado;

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getDiaDelAnio() {
        return diaDelAnio;
    }

    public void setDiaDelAnio(int diaDelAnio) {
        this.diaDelAnio = diaDelAnio;
    }

    public String getDescrpcion() {
        return descrpcion;
    }

    public void setDescrpcion(String descrpcion) {
        this.descrpcion = descrpcion;
    }

    public String getFechaFeriado() {
        return fechaFeriado;
    }

    public void setFechaFeriado(String fechaFeriado) {
        this.fechaFeriado = fechaFeriado;
    }
}
