/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class RespuestaFeriados {
    
      private String codigo ="";
    private String mensaje ="";
    private String observacion ="";
    
    private List<Feriado> listaFeriados = new ArrayList<>();

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<Feriado> getListaFeriados() {
        return listaFeriados;
    }

    public void setListaFeriados(List<Feriado> listaFeriados) {
        this.listaFeriados = listaFeriados;
    }

    
    
}
