/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class Auto {
    private String color = "";
    private int anio ;
    private String marca = "";
    private String modelo = "";
    private String placa = "";
    private List<String> listaExtras = new ArrayList<>();

    public List<String> getListaExtras() {
        return listaExtras;
    }

    public void setListaExtras(List<String> listaExtras) {
        this.listaExtras = listaExtras;
    }
    

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    
    
    
    
}
