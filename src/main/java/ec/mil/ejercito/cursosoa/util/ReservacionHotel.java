/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.util;

/**
 *
 * @author george
 */
public class ReservacionHotel {
    
    
    private String titularReserva ="";
    private String hotel ="";
    private int cantiadPersonas;
    private String fechaIngreso ="";
    private String fechaSalida ="";

    public String getTitularReserva() {
        return titularReserva;
    }

    public void setTitularReserva(String titularReserva) {
        this.titularReserva = titularReserva;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public int getCantiadPersonas() {
        return cantiadPersonas;
    }

    public void setCantiadPersonas(int cantiadPersonas) {
        this.cantiadPersonas = cantiadPersonas;
    }

  

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }
    
    
    
    
}
