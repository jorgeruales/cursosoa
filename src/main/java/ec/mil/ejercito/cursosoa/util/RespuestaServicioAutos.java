/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class RespuestaServicioAutos {
    private String codigo ="";
    private String mensaje ="";
    private String observacion ="";
    
    private List<Auto> listadoAutos = new ArrayList<>();

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<Auto> getListadoAutos() {
        return listadoAutos;
    }

    public void setListadoAutos(List<Auto> listadoAutos) {
        this.listadoAutos = listadoAutos;
    }
    
}
