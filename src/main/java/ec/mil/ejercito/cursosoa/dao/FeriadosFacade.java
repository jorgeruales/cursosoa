/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.dao;

import ec.mil.ejercito.cursosoa.modelo.Feriados;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author george
 */
@Stateless
public class FeriadosFacade extends AbstractFacade<Feriados> {

    @PersistenceContext(unitName = "cursoSOAPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FeriadosFacade() {
        super(Feriados.class);
    }
    
}
