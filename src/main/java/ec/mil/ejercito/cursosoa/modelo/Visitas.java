/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.cursosoa.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author george
 */
@Entity
@Table(name = "visitas")
@NamedQueries({
    @NamedQuery(name = "Visitas_1.findAll", query = "SELECT v FROM Visitas v")})
public class Visitas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vis_id")
    private Integer visId;
    @Size(max = 13)
    @Column(name = "vis_cliente_cedula")
    private String visClienteCedula;
    @Column(name = "vis_fecha_reserva")
    @Temporal(TemporalType.TIMESTAMP)
    private Date visFechaReserva;
    @Size(max = 24)
    @Column(name = "vis_estado_reserva")
    private String visEstadoReserva;
    @Column(name = "vis_fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date visFechaIngreso;
    @Column(name = "vis_fecha_salida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date visFechaSalida;
    @Column(name = "vis_habitaciones_reserva")
    private Integer visHabitacionesReserva;
    @Column(name = "vis_usuario_edicion")
    private Integer visUsuarioEdicion;
    @Column(name = "vis_fecha_edicion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date visFechaEdicion;

    public Visitas() {
    }

    public Visitas(Integer visId) {
        this.visId = visId;
    }

    public Integer getVisId() {
        return visId;
    }

    public void setVisId(Integer visId) {
        this.visId = visId;
    }

    public String getVisClienteCedula() {
        return visClienteCedula;
    }

    public void setVisClienteCedula(String visClienteCedula) {
        this.visClienteCedula = visClienteCedula;
    }

    public Date getVisFechaReserva() {
        return visFechaReserva;
    }

    public void setVisFechaReserva(Date visFechaReserva) {
        this.visFechaReserva = visFechaReserva;
    }

    public String getVisEstadoReserva() {
        return visEstadoReserva;
    }

    public void setVisEstadoReserva(String visEstadoReserva) {
        this.visEstadoReserva = visEstadoReserva;
    }

    public Date getVisFechaIngreso() {
        return visFechaIngreso;
    }

    public void setVisFechaIngreso(Date visFechaIngreso) {
        this.visFechaIngreso = visFechaIngreso;
    }

    public Date getVisFechaSalida() {
        return visFechaSalida;
    }

    public void setVisFechaSalida(Date visFechaSalida) {
        this.visFechaSalida = visFechaSalida;
    }

    public Integer getVisHabitacionesReserva() {
        return visHabitacionesReserva;
    }

    public void setVisHabitacionesReserva(Integer visHabitacionesReserva) {
        this.visHabitacionesReserva = visHabitacionesReserva;
    }

    public Integer getVisUsuarioEdicion() {
        return visUsuarioEdicion;
    }

    public void setVisUsuarioEdicion(Integer visUsuarioEdicion) {
        this.visUsuarioEdicion = visUsuarioEdicion;
    }

    public Date getVisFechaEdicion() {
        return visFechaEdicion;
    }

    public void setVisFechaEdicion(Date visFechaEdicion) {
        this.visFechaEdicion = visFechaEdicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (visId != null ? visId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visitas)) {
            return false;
        }
        Visitas other = (Visitas) object;
        if ((this.visId == null && other.visId != null) || (this.visId != null && !this.visId.equals(other.visId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.mil.ejercito.cursosoa.modelo.Visitas_1[ visId=" + visId + " ]";
    }
    
}
