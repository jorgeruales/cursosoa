package ec.mil.ejercito.cursosoa.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-08T11:46:25")
@StaticMetamodel(Visitas.class)
public class Visitas_ { 

    public static volatile SingularAttribute<Visitas, Date> visFechaEdicion;
    public static volatile SingularAttribute<Visitas, Date> visFechaIngreso;
    public static volatile SingularAttribute<Visitas, Integer> visHabitacionesReserva;
    public static volatile SingularAttribute<Visitas, Date> visFechaSalida;
    public static volatile SingularAttribute<Visitas, String> visClienteCedula;
    public static volatile SingularAttribute<Visitas, Integer> visId;
    public static volatile SingularAttribute<Visitas, String> visEstadoReserva;
    public static volatile SingularAttribute<Visitas, Integer> visUsuarioEdicion;
    public static volatile SingularAttribute<Visitas, Date> visFechaReserva;

}