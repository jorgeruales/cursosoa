package ec.mil.ejercito.cursosoa.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-08T11:46:25")
@StaticMetamodel(Feriados.class)
public class Feriados_ { 

    public static volatile SingularAttribute<Feriados, String> descripcion;
    public static volatile SingularAttribute<Feriados, Date> fechaEdicion;
    public static volatile SingularAttribute<Feriados, Integer> ciclo;
    public static volatile SingularAttribute<Feriados, Date> fechaFeriado;
    public static volatile SingularAttribute<Feriados, Integer> id;
    public static volatile SingularAttribute<Feriados, Integer> diaAnio;
    public static volatile SingularAttribute<Feriados, Integer> idUsuarioEdicion;

}